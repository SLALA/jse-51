package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ITokenService {

    @Nullable
    String getToken();

    void setToken(@NotNull String token);

}
