package ru.t1.strelcov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.model.SessionDTO;

public interface IAuthDTOService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    void logout(@Nullable String token);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

}
